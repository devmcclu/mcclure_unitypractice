﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawning : MonoBehaviour {

    public GameObject pickupPrefab;

    public float xMin, xMax, zMin, zMax;

    public PickupSpawning pickupSpawner;

	// Use this for initialization
	void Start () {
		
        for (int i = 0; i <= 12; i++)
        {
            SpawnPickup();
        }

        Invoke("SpawnRepeat", 5f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnRepeat()
    {
        SpawnPickup();
        Invoke("SpawnRepeat", 5f);
    }

    void SpawnPickup ()
    {
        Vector3 randomPos = new Vector3(Random.Range(xMin, xMax), pickupPrefab.transform.position.y, Random.Range(zMin, zMax));

        Instantiate(pickupPrefab, randomPos, pickupPrefab.transform.rotation);
    }
}
